    <!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Admin</title>
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


        <link rel="stylesheet" type="text/css" href="/marvel-cinema/css/marvel.css">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">


        
    </head>
    <body>
        @if(!empty(Session()) && Session::has('usertype') && Session::get('usertype') == 'Admin')

        <nav class="navbar navbar-dark bg-dark">
            <a class="navbar-brand" href="/marvel-cinema/public/members"><b>Members</b></a>
            <a class="navbar-brand" href="/marvel-cinema/public/movies"><b>Movies</b></a>
            <a class="navbar-brand" href="/marvel-cinema/public/movies_theaters"><b>Movies theaters</b></a>
            <a class="navbar-brand" href="/marvel-cinema/public/reviews"><b>Reviews</b></a>
        </nav>
        @yield('content')
        @endif
    </body>
    </html>
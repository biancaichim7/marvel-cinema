<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Movies</title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
</head>
<style>
  input[type=text], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
  }

  input[type=submit] {
    width: 100%;
    background-color: gray;
    color: black;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
  }

  input[type=submit]:hover {
    background-color: gray;
  }

  .review {
    border-radius: 5px;
    background-color: gray;
    padding: 20px;
  }

</style>
<body>
  <div class="jumbotron jumbotron-fluid">
    <div class="container-fluid jumbotron">
      <div class="row">
        <div class="col-6">
         <div class="d-flex justify-content-right col-12 p-6 embed-responsive embed-responsive-4by3 auto-width">
          {!! htmlspecialchars_decode($movie->trailer, ENT_NOQUOTES) !!} 
        </div>
      </div>

      <div class="col-6 jumbotron bg-dark">
       
        <div class="row">
          <h2 class="col-9 p-2 text-warning">{{$movie->movie_title}}</h2>
        </div>
        <br>
        <div class="col ">
          <h4 class="col-6 p-2 text-primary">Details:</h4>
          <h6 class="p-2 text-info">Release date:</h6><h6 class="p-2 text-white"> {{$movie->release_date}}</h6>
          <h6 class="p-2 text-info">Running time:</h6><h6 class="p-2 text-white">  {{$movie->running_time}} minutes</h6>
          <h6 class="p-2 text-info">Film genre:</h6><h6 class="p-2 text-white">  {{$movie->film_genre}}</h6>
          <h6 class="p-2 text-info">Cast:</h6><h6 class="p-2 text-white">  {{$movie->cast}}</h6>
          <h6 class="p-2 text-info">Director:</h6> <h6 class="p-2 text-white"> {{$movie->director}}</h6>
          <h6 class="p-2 text-info">Production:</h6><h6 class="p-2 text-white">  {{$movie->production_year}}</h6>
          <h6 class="p-2 text-info">Language:</h6><h6 class="p-2 text-white">  {{$movie->original_language}}</h6>
          <h6 class="p-2 text-info">Age restriction:</h6><h6 class="p-2 text-white">  {{$movie->age_restriction}}</h6>
          <h6 class="p-2 text-info">Price:</h6><h6 class="p-2 text-white">  {{$movie->price}} cents</h6>
        </div>
        
        <br>
        
        <div class="review">
          <form action="/marvel-cinema/public/showMovie/{id}" style="border:10px white" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="container">
              <h4 class="text-white">Give Your Review</h4>
              <hr>

              <label for="rating"><b class="text-white">Rating:</b></label> 
              <select>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
              </select>

              <label for="title"><b class="text-white">Title:</b></label>
              <input type="text" name="title" value="" id="title">

              <label for="description"><b class="text-white">Description:</b></label>
              <input type="text" name="description" value="" id="description">


              <br><br>
              <label>
                <div class="clearfix">
                  <button type="submit">Submit</button>
                </div>
              </div>
            </form>
          </div>

          <br>
          <br>
          <h3 class="text-warning">Reviews:</h3>
          <br>
          <br>
          @foreach($allReviews[$movie->id] as $review)
          <div class='row mb-1 text-white'>
            <div class='col-5'>Title:</div>
            <div class='col-7'>{{ $review->title }}</div>
          </div>
          <div class='row mb-1 text-white'>
            <div class='col-5'>Rating:</div>
            <div class='col-7'>{{ $review->rating }}</div>
          </div>
          <div class='row mb-1 text-white'>
            <div class='col-5'>Description:</div>
            <div class='col-7'>{{ $review->description }}</div>
          </div>
          <hr>
          @endforeach
          <span class="p-2 d-flex justify-content-center">
            {!!Html::link("/",'Back', ['class' => 'btn btn-primary'])!!}
          </span>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
